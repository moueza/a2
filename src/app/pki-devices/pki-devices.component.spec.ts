import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PkiDevicesComponent } from './pki-devices.component';

describe('PkiDevicesComponent', () => {
  let component: PkiDevicesComponent;
  let fixture: ComponentFixture<PkiDevicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PkiDevicesComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PkiDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
