import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PolarDevicesComponent } from './polar-devices.component';

describe('PolarDevicesComponent', () => {
  let component: PolarDevicesComponent;
  let fixture: ComponentFixture<PolarDevicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PolarDevicesComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PolarDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
